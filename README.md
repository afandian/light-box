# Light Box

An LED-button isometric grid. Using ESP32 board and HT1633 I2C Matrix driver. 

Not much say - connects via I2C using configurable pins, a 5 x 11 grid is connected to the GPIO pins. Note that some of the pins chosen are input-only and don't have pull-ups, so they must be supplied.

Uses the Arduino ESP32 core.

Full story at <https://blog.afandian.com/2020/02/light-box-heavy-times/>

<img src="lightbox.jpg">