#include <Wire.h>
#include <time.h>
#include <math.h>
#include "esp32-hal-cpu.h"

// Some of these consts are passed into inits, some are used for static dimensions.
// Bit inconsistent.

// Pixel dimensions (how many LEDs fit in the space).
// A subset than what's available to the HT16K33.
// Also corresponds to the keyboard size.
const int NUM_XS = 5;
const int NUM_YS = 11;

int KEY_XS[] = {16, 17, 5, 18, 19};
int KEY_YS[] = {12, 14, 27, 26, 25, 33, 32, 35, 34, 39, 36};

// I2C address of HT16K33.
const uint8_t I2C_ADDR = 0x70;
const int I2C_SPEED = 400000;
const int SDA_PIN = 22;
const int SCL_PIN = 23;

// Number of columns in HT16K33 memory (each being 16 bits high).
const int BUF_XS = 8;

// Buffer for HT16K33.
class Buffer  {
    int xs;
    int ys;

  public:
    uint16_t buf[BUF_XS];

    void init(int xs, int ys) {
      this->xs = xs;
      this->ys = ys;
      clear();
    }

    void set(int x, int y, bool value) {
      if (x > xs) {
        return;
      }

      if (y > ys) {
        return;
      }

      // Adjust for pin layout!
      if (y == 10) {
        y = 0;
      } else {
        y += 1;
      }

      if (value) {
        buf[x] |= (1 << y);
      } else {
        buf[x] &= ~(1 << y);
      }
    }

    bool get(int x, int y) {
      if (x > xs) {
        return false;
      }

      if (y > ys) {
        return false;
      }

      return buf[x] & (1 << y);
    }

    void inspect_buf() {
      Serial.println("Inspect...");
      for (int x = 0; x < BUF_XS; x++) {
        Serial.printf("%#08x %#08x \n", (buf[x] & 0xFF), buf[x] >> 8);
      }
    }

    // OR this buffer with another one.
    void composite(Buffer* other) {
      for (int x = 0; x < BUF_XS; x++) {
        buf[x] = buf[x] | other->buf[x];
      }
    }

    void clear() {
      for (int x = 0; x < BUF_XS; x++) {
        buf[x] = 0x0000;
      }
    }

    void fill() {
      for (int x = 0; x < BUF_XS; x++) {
        buf[x] = 0xFFFF;
      }
    }
};

// A buffered display connected to a HT16K33.
// Maintain a 'source' buffer.
class Grid {
  private:
    // i2c address
    int address;
    int sda;
    int scl;

    int numXs;
    int numYs;

  public:
    // Source frame buffer - the input from the application.
    Buffer source;

    Grid() {

    }

    void init(int address, int sda, int scl, int numXs, int numYs) {
      this->address = address;
      this->scl = scl;
      this->sda = sda;

      this->numXs = numXs;
      this->numYs = numYs;

      source.init(numXs, numYs);

      // It's important to specify the speed explicitly.
      Wire.begin(sda, scl, I2C_SPEED);

      // "System setup" turn on oscillator.
      Wire.beginTransmission(this->address);
      Wire.write(0x20 | 1);
      Wire.endTransmission();

      // "Display setup" on.
      Wire.beginTransmission(this->address);
      Wire.write(0x81);
      Wire.endTransmission();
    }

    void set(int x, int y, bool value) {
      source.set(x, y, value);
    }

    void send() {
      Wire.beginTransmission(address);
      // Start at byte 0, send whole screen.
      Wire.write(0x00);

      for (int x = 0; x < BUF_XS; x++) {
        Wire.write(source.buf[x] & 0xFF);
        Wire.write(source.buf[x] >> 8);
      }

      Wire.endTransmission();
    }

    void inspect_buf() {
      source.inspect_buf();
    }

    void fill() {
      source.fill();
    }

    void clear() {
      source.clear();
    }
};

class Keys {
  private:
    // Row and col pins.
    int *xs;
    int *ys;
    int numXs;
    int numYs;

  public:

    // The current state as of last sample.
    bool state[NUM_XS][NUM_YS];

    // The previous state. Used to diff.
    bool prev[NUM_XS][NUM_YS];


    void init(int *xs, int numXs, int *ys, int numYs) {
      this->xs = xs;
      this->ys = ys;
      this->numXs = numXs;
      this->numYs = numYs;

      for (int x = 0; x < numXs; x++) {
        for (int y = 0; y < numYs; y++) {
          state[x][y] = false;
          prev[x][y] = false;
        }
      }

      // Setup GPIO pins.
      for (int x = 0; x < numXs; x++) {
        int xPin = xs[x];
        Serial.printf("Set input %d \n", xPin);
        pinMode(xPin, OUTPUT);
        digitalWrite(xPin, LOW);
        Serial.println("Done");
      }

      for (int y = 0; y < numYs; y++) {
        int yPin = ys[y];
        Serial.printf("Set output %d \n", yPin);
        pinMode(yPin, INPUT_PULLDOWN);
        Serial.println("Done");
      }
    }


    void scan() {
      for (int x = 0; x < numXs; x++) {
        int xPin = xs[x];
        digitalWrite(xPin, HIGH);
        delay(5); // TODO

        for (int y = 0; y < numYs; y++) {
          int yPin = ys[y];
          int val = digitalRead(yPin);
          state[x][y] = val;
        }
        digitalWrite(xPin, LOW);
      }
    }

    void inspect() {
      for (int x = 0; x < numXs; x++) {
        Serial.printf("%2d :", x);
        for (int y = 0; y < numYs; y++) {
          Serial.printf("%2d  ", state[x][y]);
        }
        Serial.printf("\n");
      }
    }

    bool getState(int x, int y) {
      if (x > NUM_XS) {
        return false;
      }

      if (y > NUM_YS) {
        return false;
      }

      return state[x][y];
    }

    bool changed(int x, int y) {
      if (x > NUM_XS) {
        return false;
      }

      if (y > NUM_YS) {
        return false;
      }

      return state[x][y] != prev[x][y];
    }

    // Reset the 'previous' snapshot to the current state.
    void resetPrev() {
      for (int x = 0; x < numXs; x++) {
        for (int y = 0; y < numYs; y++) {
          prev[x][y] = state[x][y];
        }
      }
    }
};

// What mode are we in?
enum Mode {
  // Press a button, it inverts.
  mode_canvas,

  // Whack-a-mole game.
  mode_game
};

struct coord {
  int x;
  int y;
};

class Canvas {
  private:
    bool canvas[NUM_XS][NUM_YS];
    Keys keys;
    Grid grid;
    Mode mode;

    clock_t lastTick;

    // How many turns have been played on this game?
    int gameTurns;

  public:
    void init() {
      Serial.println("Init canvas..");

      mode = mode_canvas;
      lastTick = clock();

      keys.init(KEY_XS, NUM_XS, KEY_YS, NUM_YS);
      grid.init(I2C_ADDR, SDA_PIN, SCL_PIN, NUM_XS, NUM_YS);

      grid.fill();
      grid.send();
      delay(500);
      grid.clear();
      grid.send();

      Serial.println("Blank canvas..");
      // Blank canvas.
      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          canvas[x][y] = false;
        }
      }

      grid.clear();
      grid.send();

      Serial.println("Done init");
    }

    int getNumOff() {
      int result = 0;
      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          if (!canvas[x][y]) {
            result++;
          }
        }
      }

      return result;
    }

    // Choose the coordinate of a random unset pixel.
    // Return (-1, -1) if all are set.
    coord random_coord() {
      // pairs of x, y as yet unset.
      coord options[NUM_XS * NUM_YS];

      int num_options = 0;

      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          if (!canvas[x][y]) {
            options[num_options] = coord{x, y};
            num_options++;
          }
        }
      }

      // If there are none left, say so;
      if (num_options == 0) {
        return coord{ -1, -1};
      }

      int choice = random(num_options);
      return options[choice];
    }

    void demo() {
      grid.fill();
      grid.send();
      delay(100);
      grid.clear();
      grid.send();


      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          grid.set(x, y, true);
          grid.send();
          delay(10);
        }
      }

      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          grid.set(x, y, false);
          grid.send();
          delay(10);
        }
      }

      // Be sure that we end on empty.
      grid.clear();
      grid.send();
    }



    void tick() {
      clock_t now = clock();
      long tickDiff = (long) (now - lastTick);

      Serial.printf("tick %d \n", tickDiff);
      // Scan the keyboard, respond to changes.
      keys.scan();

      // Ticks can pass, if the timer is to low,
      // i.e. waiting a bit longer.
      bool acceptedTick = false;
      if (mode == mode_canvas) {
        acceptedTick = canvas_tick(tickDiff);
      } else if (mode == mode_game) {
        acceptedTick = game_tick(tickDiff);
      }

      keys.resetPrev();

      // Only update the last tick if it was consumed.
      if (acceptedTick) {
        lastTick = now;
      }
    }

    void enterGame() {
      gameTurns = 0;
      mode = mode_game;
    }

    void enterCanvas() {
      // Clear grid so that it starts in an empty state.
      // Otherwise it could trigger to enter the game state again.
      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          canvas[x][y] = false;
        }
      }

      mode = mode_canvas;

    }

    // Event that a button was pressed.
    bool canvas_tick(long tickDiff) {

      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          if (keys.changed(x, y)) {
            bool state = keys.getState(x, y);
            if (state) {
              canvas[x][y] = !canvas[x][y];
              grid.set(x, y, canvas[x][y]);
            }
          }
        }
      }

      grid.send();


      // Switch into game mode if bottom left and top right corners are pressed.
      if (keys.getState(4, 10) && keys.getState(0, 0)) {
        enterGame();
        return true;
      }

      return true;
    }

    bool game_tick(long tickDiff) {

      int turnsLeft = max(1, (NUM_XS * NUM_YS) - gameTurns);

      int wantedDelay = 100 + (turnsLeft * random(1000));
      bool waitedLongEnough = tickDiff > wantedDelay;

      // Take a turn if we've waited long enough.
      if (waitedLongEnough) {
        gameTurns ++;

        coord choice = random_coord();
        Serial.printf("Choice %d,%d \n", choice.x, choice.y);

        // If there are none left, celebrate!
        if (choice.x == -1) {
          Serial.printf("Done game!");

          // Flashing!
          demo();

          enterCanvas();
          return true;
        }

        // Set the random one.
        canvas[choice.x][choice.y] = true;
      }

      // Always respond to keys.
      // If any keys are pressed, set them off.
      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          bool state = keys.getState(x, y);
          if (state) {
            canvas[x][y] = false;
          }
        }
      }

      // Exit into game mode if bottom left and top right corners are pressed.
      // Only do this if we've taken a turn or we bounce right out of game state as soon as we enter it!
      if (gameTurns > 2 && keys.getState(0, 10) && keys.getState(4, 0)) {
        enterCanvas();
        return true;
      }

      // Then paint grid from canvas.
      for (int x = 0; x < NUM_XS; x++) {
        for (int y = 0; y < NUM_YS; y++) {
          grid.set(x, y, canvas[x][y]);
        }
      }
      grid.send();


      // Only return that we consumed the tick if we waited long enough.
      // So that the tick counter doesn't get reset.
      return waitedLongEnough;
    }
};

Canvas canvas;

void setup() {
  Serial.begin(115200);

  // Slow as possible for battery.
  setCpuFrequencyMhz(10);
  Serial.printf("CPU: %d \n", getCpuFrequencyMhz());

  canvas.init();

  canvas.demo();
}

void loop() {
  canvas.tick();
  delay(10);
}
